1) To fetch a set of data from a remote API endpoint, then to store that data into redux and then to present it on the screen you should...

Assuming you are using structure as

React, Redux and Redux-Saga

1. Dispatch the action from somewhere to fetch data, in this application it is being dispatched when rendering `Posts`

2. While redux-saga watch function is processing, it goes through reducer and do the changes according to the action dispatched, in this case set `isFetching` to `true`

3. Redux-Saga takes the action dispatched in parallel to process it. It makes the request, wait the response and, then dispatch a success or failed action

4. Re-render component with the new state, after `store.state` change (effect from 2), so display 'Loading...'

5. Pass through the reducer again with the action type dispatched from saga (effect from 3), in this case it will set the `state.data` attribute with `posts`

6. After state changes (effect from 5), re-render component with new state, then display all posts


2) Utilities (./utils) has a `generateUrl` to generate a URL from given parameters:
```js
width: 360,
height: 300,
locale: 'en',
toolbar_bg: '',
interval: '3h',
pair: 'BTC_USD',
```

You can use any lib but the generated result should be
`"http://testurl.bitfinx.com/?height=300&interval=3h&locale=en&pair=BTC_USD&width=360"`

More parameters are planned to be added/removed later and the the result should neglect the empty params (ex: should not include toolbar_bg in URL when its value is empty).

3) Utilities (./utils) has the refactoring for the funciton bellow.

```js
var volumeSetup = function () {
// setup volume unit interface
var volumeUnit = window.APP.util.getSettings('ticker_vol_unit').toUpperCase();
var element = null;
if (volumeUnit === 'FIRSTCCY') {
element = $('#tickervolccy_0');
} else if (volumeUnit === 'USD') {
element = $('#tickervolccy_USD');
} else if (volumeUnit === 'BTC') {
element = $('#tickervolccy_BTC');
} else if (volumeUnit === 'ETH') {
element = $('#tickervolccy_ETH');
}
if (element) {
element.prop("checked", true);
}
// override currencies list
var result = window.APP.util.initCurrenciesList()
return result
}
```

## Up and Running

1. Clone repo
2. `yarn install`
3. `yarn start`
4. access localhost:3000

## Testing
`yarn test`
