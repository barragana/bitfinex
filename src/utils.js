/**
 *
 * @param {Object} params attributes com compose the url
 * In this case, it is better option to use an object to pass
 * all attributes because the order and names won't affect,
 * therefore can be changed easily in the future
 */

export function generateUrl(params) {
  return Object
    .keys(params)
    .sort()
    .reduce((res, key, index) => {
      let and = '&';
      if (!index) { and = ''; }
      if(!params[key]) return res;
      return `${res}${and}${key}=${params[key]}`;
    }, `http://testurl.bitfinx.com/?`);
}
/**
 * Clean code,
 * Extract coins x element id into a constant
 *    With that there is no need to do any ifs,
 *    just a access the attribute
 */
const coinsElementId = {
  FIRSTCCY: '#tickervolccy_0',
  USD: '#tickervolccy_USD',
  BTC: '#tickervolccy_BTC',
  ETH: '#tickervolccy_ETH',
};
export function volumeSetup() {
  const volumeUnit = window.APP.util.getSettings('ticker_vol_unit').toUpperCase();
  const element = $(coinsElementId[volumeUnit]);
  if (element) {
    element.prop("checked", true);
  }
  return window.APP.util.initCurrenciesList();
}
