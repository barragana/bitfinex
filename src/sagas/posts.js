import { all, takeLatest, put, call } from 'redux-saga/effects';

import {
  FETCH_POSTS_REQUESTED,
  fetchPostsSuccessed,
  fetchPostsFailed
} from '../ducks/posts';
import request from '../api/request';

export function* handleFetchData() {
  try {
    console.log('>>>> 3');
    const posts = yield call(
      request,
      '/posts',
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        },
      },
    );
    yield put(fetchPostsSuccessed(posts));
  } catch (e) {
    yield put(fetchPostsFailed(e.errors));
  }
}

export default function* watchData() {
  yield all([takeLatest(FETCH_POSTS_REQUESTED, handleFetchData)]);
}
