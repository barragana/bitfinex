async function request(url, options) {
  const path = `${process.env.REACT_APP_API_URL}${url}`;
  try {
    const res = await fetch(`${path}`, options);
    const result = await res.json();

    if (!res.ok) {
      const error = new Error(res.statusText);
      error.status = res.status;
      throw error;
    }

    return result;
  } catch (error) {
    throw error;
  }
}

export default request;
