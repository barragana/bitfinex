import { combineReducers } from 'redux';

import posts from './posts';

const rootDucks = combineReducers({
  posts,
});

export default rootDucks;
