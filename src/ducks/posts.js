
export const FETCH_POSTS_REQUESTED = 'FETCH_POSTS_REQUESTED';
export const fetchPostsRequested = () => ({ type: FETCH_POSTS_REQUESTED });

export const FETCH_POSTS_SUCCESSED = 'FETCH_POSTS_SUCCESSED';
export const fetchPostsSuccessed = posts => ({ type: FETCH_POSTS_SUCCESSED, payload: { posts } });

export const FETCH_POSTS_FAILED = 'FETCH_POSTS_FAILED';
export const fetchPostsFailed = error => ({ type: FETCH_POSTS_FAILED, payload: { error } });

export const initialState = {
  isFetching: false,
  didInvalidate: false,
  data: [],
  error: null,
};

const postsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_POSTS_REQUESTED:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
        error: null,
      };

    case FETCH_POSTS_SUCCESSED:
      return {
        ...state,
        isFetching: false,
        data: payload.posts,
      };

    case FETCH_POSTS_FAILED:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
        error: payload.error,
      };

    default:
      return state;
  }
};

export default postsReducer;
