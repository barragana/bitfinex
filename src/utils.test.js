const { generateUrl } = require("./utils")

describe('Utils', () => {
  describe('generateUrl should', () => {
    test('generate url with no empty values', () => {
      const url = generateUrl({
        width: 360,
        height: 300,
        locale: 'en',
        toolbar_bg: '',
        interval: '3h',
        pair: 'BTC_USD'
      });
      expect(url).toBe('http://testurl.bitfinx.com/?height=300&interval=3h&locale=en&pair=BTC_USD&width=360');
    });
  })
})
