import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PostList from '../components/PostsList';
import { fetchPostsRequested } from '../ducks/posts';

const Posts = ({ isFetching, didInvalidate, data, fetchPostsRequested }) => {
  useEffect(() => {
    console.log('>>>> 1');
    fetchPostsRequested();
  }, []);

  if (isFetching) {
    console.log('>>>> 4');
    return 'Loading...';
  }

  if (didInvalidate) return 'Something went wrong.';

  if (!data.length) return false;

  console.log('>>>> 6');
  return <PostList posts={data} />;
};

const mapStateToProps = ({ posts }) => ({ ...posts });

const mapDispatchToProps = {
  fetchPostsRequested,
};
export default connect(mapStateToProps, mapDispatchToProps)(Posts);
