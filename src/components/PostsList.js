import React from 'react';

const containerStyle = {
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'center',
  margin: '36px',
};

const bodyStyle = {
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
}

const titleStyle = {
  borderBottom: '1px solid black',
  padding: '4px',
  width: '100%',
  ...bodyStyle,
};

const postStyle = {
  border: '1px solid grey',
  display: 'flex',
  flexWrap: 'wrap',
  margin: '4px',
  width: '200px',
}
const PostList = ({ posts }) => (
  <div style={containerStyle}>
    {posts.map(({ id, title, body }) => (
      <div key={id} style={postStyle}>
        <span style={titleStyle}>{title}</span>
        <span style={bodyStyle}>{body}</span>
      </div>
    ))}
  </div>
);

export default PostList;

